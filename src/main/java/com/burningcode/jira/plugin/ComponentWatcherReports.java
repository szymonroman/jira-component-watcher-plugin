package com.burningcode.jira.plugin;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import java.util.List;

public class ComponentWatcherReports extends JiraWebActionSupport {
    public List<Project> getProjects() {
        return getProjectManager().getProjects();
    }
}
