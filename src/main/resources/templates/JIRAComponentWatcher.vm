<!--
 * Copyright (c) 2008, 2009, Ray Barham
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Ray Barham ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ray Barham BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-->
 
<!--
 Author: Ray Barham
 Description: This is the velocity template used to display all the component watchers 
 on a project and gives the option to edit the watchers. 
-->

#disable_html_escaping()
<html>
<head>
    <title>${i18n.getText("action.jiracomponentwatcher.title")} - ${action.project.name}</title>
    $webResourceManager.requireResource("com.burningcode.jira.plugin.jiracomponentwatcher:admin-resources")
    <meta name="decorator" content="alt.admin"/>
    <meta name="projectKey" content="${action.projectKey}"/>
    <meta name="projectId" content="${action.projectId}"/>
    <meta name="admin.active.tab" content="edit_component_watchers"/>
    <meta name="admin.active.section" content="atl.jira.proj.config"/>
</head>
<body>
	<section id="content" role="main">
	    <header class="aui-page-header">
	        <div class="aui-page-header-inner">
	            <div class="aui-page-header-image">
	                <div class="aui-avatar aui-avatar-xlarge aui-avatar-project">
	                    <div class="aui-avatar-inner project-config-icon48-notifications"></div>
	                </div>
	            </div>
	            <div class="aui-page-header-main">
	                <h1>${i18n.getText("action.jiracomponentwatcher.form.title")}</h1>
	                <p>${i18n.getText("action.jiracomponentwatcher.form.desc", ${action.project.name})}</p>
	                <p>${i18n.getText("action.jiracomponentwatcher.form.desc2")}</p>
	            </div>
	            <div class="aui-page-header-actions">
	                <div class="aui-buttons">
	                    <a class="aui-button" href="$baseurl/plugins/servlet/project-config/${action.projectKey}/notifications">
	                    	${i18n.getText("admin.schemes.notifications.notifications")}
                    	</a>
	                </div>
	            </div>
	        </div>
	    </header>
		#if($action.projectId)
			#set ($components = $action.components)
		
			#if($components)
			    <div class="aui-page-panel">
			        <div class="aui-page-panel-inner">
			            <section class="aui-page-panel-content">
						    <table id="component_watchers_table" class="aui aui-table-rowhover">
						    	<thead>
						    		<tr>
							    		<th><b>${i18n.getText("admin.projects.components")}</b></th>
					                    <th><b>${i18n.getText("admin.projects.component.lead")}</b></th>
							    		<th><b>${i18n.getText("admin.common.words.users")}</b></th>
							    		<th><b>${i18n.getText("common.words.groups")}</b></th>
							    		<th><b>${i18n.getText("common.words.operations")}</b></th>	
						    		</tr>
						    	</thead>
						    	<tbody>
					    		#foreach($component in $components)
									<tr> 
										<td>
						    				$component.name
							    		</td>
					                    <td>
					                        #if($component.lead)
												${userformat.formatUserkey($component.lead, 'profileLinkActionHeader', "component_${component.id}_lead")}
					                        #else
												None
					                        #end
					                    </td>
							    		<td>
							    			<ul>
								    			#set ($userWatchers = $action.getUserWatchers($component.id))
								    			#foreach($watcher in $userWatchers)
								    				<li>${userformat.formatUserkey($watcher.key, 'profileLinkActionHeader', "component_${component.id}_user_watcher_${watcher.name}")}</li>
								    			#end
								    		</ul>			    				
							    		</td>
							    		<td>
							    			<ul>
								    			#set ($groupWatchers = $action.getGroupWatchers($component.id))
								    			#foreach($watcher in $groupWatchers)
								    				<li id="component_${component.id}_group_watcher_${watcher.name}">${watcher.name}</li>
								    			#end
								    		<ul>			
							    		</td>
							    		<td>
							    			<ul class="operations-list">
							    				<li>
							    					<a id="editUserWatchers_$component.id" href="$baseurl/secure/project/EditComponentUserWatcher.jspa?componentId=$component.id">
							    						${i18n.getText("action.jiracomponentwatcher.form.users")}
						    						</a>
						    					</li>
							    				<li>
							    					<a id="editGroupWatchers_$component.id" href="$baseurl/secure/project/EditComponentGroupWatcher.jspa?componentId=$component.id">
								    					${i18n.getText("action.jiracomponentwatcher.form.groups")}
							    					</a>
							    				</li>
						    				</ul>	
							    		</td
						    		</tr>
					    		#end
					    		</tbody>
							</table>
						</section>
					</div>
				</div>
			#else
				<div class="aui-message warning">
				    <p class="title">
				        <span class="aui-icon icon-warning"></span>
				        <strong>No components provided</strong>
				    </p>
				    <p>Something went wrong.  No components were provided.  This should never happen.</p>
				</div>
			#end
		#else
			<div class="aui-message warning">
			    <p class="title">
			        <span class="aui-icon icon-warning"></span>
			        <strong>No project provided</strong>
			    </p>
			    <p>Something went wrong.  No project was provided.  This should never happen.</p>
			</div>
		#end
	</section>
</body>
</html>