package it.com.burningcode.jira.plugin.customfields;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_USERNAME;
import static it.com.burningcode.jira.IntegrationTestHelper.*;

@Restore("WithComponentWatchers.zip")
@LoginAs(user = ADMIN_USERNAME)
public class ComponentWatchersFieldTypeTest extends BaseJiraFuncTest {
    public static final String FIELD_NAME = "Component Watchers";
    public static final String REINDEX_TEXT = "perform a re-index";

    @Test
    public void testAddingCustomField() {
        addCustomField();

        navigation.gotoCustomFields();
        assertions.getTextAssertions().assertTextPresent(FIELD_NAME);
    }

    @Test
    public void testDisplayReindexMessageOnAddingWatchersWhenFieldExists() {
        addCustomField();

        gotoEditComponentWatchers(PROJECT_KEY);

        assertions.getTextAssertions().assertTextNotPresent(REINDEX_TEXT);

        addWatcher(COMPONENT_CODE_ID, BOB_USERNAME);

        assertions.getTextAssertions().assertTextPresent(REINDEX_TEXT);
    }

    @Test
    public void testDisplayReindexMessageOnRemovingWatchersWhenFieldExists() {
        addCustomField();

        gotoEditComponentWatchers(PROJECT_KEY);

        assertions.getTextAssertions().assertTextNotPresent(REINDEX_TEXT);

        removeWatcher(COMPONENT_ART_ID, ADMIN_USERNAME);

        assertions.getTextAssertions().assertTextPresent(REINDEX_TEXT);
    }

    @Test
    public void testNotDisplayReindexMessageOnAddingWatchersWhenFieldNotExists() {
        gotoEditComponentWatchers(PROJECT_KEY);

        assertions.getTextAssertions().assertTextNotPresent(REINDEX_TEXT);

        addWatcher(COMPONENT_CODE_ID, BOB_USERNAME);

        assertions.getTextAssertions().assertTextNotPresent(REINDEX_TEXT);
    }

    @Test
    public void testNotDisplayReindexMessageOnRemovingWatchersWhenFieldNotExists() {
        gotoEditComponentWatchers(PROJECT_KEY);

        assertions.getTextAssertions().assertTextNotPresent(REINDEX_TEXT);

        removeWatcher(COMPONENT_ART_ID, ADMIN_USERNAME);

        assertions.getTextAssertions().assertTextNotPresent(REINDEX_TEXT);
    }

    private void addCustomField() {
        backdoor.customFields().createCustomField(FIELD_NAME, null, FIELD_TYPE, FIELD_SEARCH_TYPE);
    }

    private void addWatcher(final int componentId, final String watcher) {
        tester.clickLink("editUserWatchers_" + componentId);
        tester.getDialog().setFormParameter("watcherField", watcher);
        tester.getDialog().submit("add_watchers");
    }

    private void gotoEditComponentWatchers(final String projectKey) {
        navigation.gotoPage("/plugins/servlet/project-config/" + PROJECT_KEY);
        navigation.clickLinkWithExactText(COMPONENT_WATCHERS_LINK);
    }

    private void removeWatcher(final int componentId, final String watcher) {
        tester.clickLink("editUserWatchers_" + componentId);
        tester.checkCheckbox("removeWatcher_" + watcher);
        tester.getDialog().submit("remove_watchers");
    }
}
